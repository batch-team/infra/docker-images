# Docker Images

These are container images for utility and basic containers. The provided GitLab CI/CD provides the build and deployment of container images based on changes to the changes in Dockerfiles.

## Sample Web Server: Hello Batch

hello-batch folder contains a minimum web container used by hello-batch Helm charts as a sanity check.
