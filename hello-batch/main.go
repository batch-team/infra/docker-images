package main

import (
        "os"
        "fmt"
        "io"
        "time"
        "log"
        "net/http"
        "net/http/httputil"
        "html/template"
        "github.com/prometheus/client_golang/prometheus/promhttp"
)

const STATIC_URL  string = "/static/"
const STATIC_ROOT string = "static/"

type Context struct {
  Title  string
  Static string
  Username string
  Picture string
  Subtitle string
}

func Home(w http.ResponseWriter, req *http.Request) {
  context := Context{Title: "Home"}
  context.Static = STATIC_URL

  /* Check that username has been specified */
  if len(req.Header["X-Krb-User"]) != 0 {
    context.Username = req.Header.Get("X-Krb-User")
    context.Picture  = "robot_happy.png"
    context.Subtitle = "The web server is happy to visit with an authenticated user"
  } else {
    context.Username = "human"
    context.Picture  = "robot.png"
    context.Subtitle = "Here we have a sad and lonely web server"
  }

  t, err := template.ParseFiles("index.html")
  if err != nil { log.Print("Failed to parse index.html template: ",err) }

  err = t.Execute(w,context)
  if err != nil { log.Print("Failed to execute template: ",err) }

  fmt.Printf("Served: %s\n",req.Host)

  // Print out the request with the headers
  requestDump, err := httputil.DumpRequest(req, true)
  if err != nil { fmt.Println(err) }
  fmt.Println(string(requestDump))
}

/* Handling static resources */
func StaticHandler(w http.ResponseWriter, req *http.Request) {

  static_file := req.URL.Path[len(STATIC_URL):]
  if len(static_file) != 0 {
    f, err := http.Dir(STATIC_ROOT).Open(static_file)
    if err == nil {
      content := io.ReadSeeker(f)
      http.ServeContent(w, req, static_file, time.Now(), content)
      return
    }
  }
  http.NotFound(w, req)
}

func main() {
  arguments := os.Args
  PORT := ":" + arguments[1]

  http.HandleFunc("/",Home)
  http.Handle("/metrics", promhttp.Handler())
  http.HandleFunc(STATIC_URL, StaticHandler)

  err := http.ListenAndServe(PORT,nil)
  if err != nil { log.Fatal("ListenAndServe: ", err) } 
}
