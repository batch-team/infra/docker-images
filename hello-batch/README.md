# hello-batch

Minimal website webserver for testing web functionality within Kubernetes. 

## Local Execution

To build and run locally use the following commands making sure to specify the port:

```
~$ go build
~$ ./web-server 8081
```
