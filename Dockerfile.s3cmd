# Builder image to fetch and unpack binaries.
FROM alpine:latest as build

ENV KUBE_LATEST="v1.21.0"
ENV HELM_LATEST="v2.17.0"
ENV S3_LATEST="2.1.0"

WORKDIR /

RUN apk add --update --no-cache ca-certificates && apk add --update -t deps curl tar \
  && curl -L https://storage.googleapis.com/kubernetes-release/release/${KUBE_LATEST}/bin/linux/amd64/kubectl -o /tmp/kubectl \
  && chmod +x /tmp/kubectl \
  && curl -L https://storage.googleapis.com/kubernetes-helm/helm-${HELM_LATEST}-linux-amd64.tar.gz | tar zxv -C /tmp \
  && curl -L http://downloads.sourceforge.net/project/s3tools/s3cmd/${S3_LATEST}/s3cmd-${S3_LATEST}.tar.gz | tar zxv -C /tmp

# Minimum image meant for minimal execution.
FROM alpine:latest

ENV S3_LATEST="2.1.0"

RUN apk add --update --no-cache ca-certificates python3 py-dateutil git curl bash

COPY --from=build /tmp/linux-amd64/helm /bin/helm
COPY --from=build /tmp/linux-amd64/tiller /bin/tiller
COPY --from=build /tmp/s3cmd-${S3_LATEST}/s3cmd /bin/s3cmd
COPY --from=build /tmp/s3cmd-${S3_LATEST}/S3 /bin/S3
COPY --from=build /tmp/kubectl /bin/kubectl
